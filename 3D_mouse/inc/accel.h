#ifndef ACCEL_H
#define ACCEL_H

#include <stdio.h>

#include "tm_stm32f4_mpu6050.h"
#include "leds.h"
#include "filter.h"
#include "time.h"



typedef struct {
	float gyroMult;          /*!< Gyroscope corrector from raw data to "degrees/s". Only for private use */
	float acceMult;          /*!< Accelerometer corrector from raw data to "g". Only for private use */

	float accelX; 		       /*!< Accelerometer value X axis */
	float accelY; 		       /*!< Accelerometer value Y axis */
	float accelZ;	 	         /*!< Accelerometer value Z axis */
	float gyroX;     	       /*!< Gyroscope value X axis */
	float gyroY;    	       /*!< Gyroscope value Y axis */
	float gyroZ;     	       /*!< Gyroscope value Z axis */
	float temper;       	   /*!< Temperature in degrees */
} sensorValue;


void initIMU(TM_MPU6050_Device_t );


void readIMU(TM_MPU6050_t *);


void getRealValues(TM_MPU6050_t *,sensorValue *);


void returnOrientation(float *, float *, TM_MPU6050_Device_t);

#endif
