#ifndef CALIB_H
#define CALIB_H

#include <stdio.h>
//#include "stm32f4xx.h"
#include "stm32f4xx_gpio.h"
#include "stm32f4xx_rcc.h"
#include "stm32f4xx_exti.h"
#include "misc.h"
#include "stm32f4xx_syscfg.h"

#include "accel.h"
#include "leds.h"



void initButton();

void calibrate(TM_MPU6050_Device_t);

void returnCalibratedOrientation(float *, float *, TM_MPU6050_Device_t);


#endif
