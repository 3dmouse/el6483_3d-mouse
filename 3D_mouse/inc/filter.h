#ifndef FILTER_H
#define FILTER_H

#include <math.h>

#include "time.h"
#include "tm_stm32f4_mpu6050.h"



void complementaryFilter(int16_t *, int16_t *, float *, float *, TM_MPU6050_Device_t);

#endif
