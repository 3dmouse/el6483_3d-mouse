#ifndef LEDS_H
#define LEDS_H

#include "stm32f4xx.h"
#include "stm32f4xx_gpio.h"
#include "stm32f4xx_rcc.h"
#include "stm32f4xx_tim.h"
#include "stm32f4xx_exti.h"
#include "misc.h"
#include "stm32f4xx_syscfg.h"

#define LED_ALL  GPIO_Pin_12 | GPIO_Pin_13 | GPIO_Pin_14 | GPIO_Pin_15
#define LED_GREEN   GPIO_Pin_12 		// Green LEDS - left
#define LED_ORANGE  GPIO_Pin_13 		// Orange LEDS - up
#define LED_RED     GPIO_Pin_14 		// Red LEDS - right
#define LED_BLUE    GPIO_Pin_15 		// Blue LEDS - down

void initLeds();
void initTimer2();
void init_BlinkTwice(uint16_t);
void init_Blink(uint16_t);
void init_TripleBlink(uint16_t);
void disp_error();
void init_SlowBlink(uint16_t);
void stop_Blink();

#endif
