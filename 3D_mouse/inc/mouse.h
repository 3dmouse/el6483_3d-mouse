#ifndef MOUSE_H
#define MOUSE_H

#include "tm_stm32f4_usb_hid_device.h"
#include "time.h"
#include "leds.h"

int initHIDMouse(TM_USB_HIDDEVICE_Mouse_t *);
int sendMouseDescriptor(TM_USB_HIDDEVICE_Mouse_t *);

#endif
