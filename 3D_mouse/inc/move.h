#ifndef MOVE_H
#define MOVE_H

#include "mouse.h"
#include "accel.h"
#include "calib.h"
#include "stm32f4xx_tim.h"

#include <math.h>

typedef enum {
	mpitch = 0x00,
	mroll = 0x01
} a;

typedef enum {
	roamer = TM_MPU6050_Device_0,
	clicker = TM_MPU6050_Device_1
} mpu;

void initMove();

void initTimer4();

void getMouseMoment(TM_USB_HIDDEVICE_Mouse_t *);

int whereMovement(a , mpu );

int8_t scale(int );

int isLeftButtonPressed();

int isRightButtonPressed();


#endif
