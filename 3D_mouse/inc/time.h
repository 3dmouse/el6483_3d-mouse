#ifndef TIME_H
#define TIME_H

#define INTERVAL 		 1
#define TIMECONST		 99
#define PI			     3.141592

#include "stm32f4xx.h"

static volatile uint32_t 	millisecondCounter;

void initSystick();

void delay(uint32_t );

#endif
