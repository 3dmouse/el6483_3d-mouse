#include "accel.h"


int16_t accelR1[3]={0,0,0};
int16_t gyroR1[3]={0,0,0};
TM_MPU6050_t 			rawRoamerStruct;
TM_MPU6050_t 			rawClickerStruct;

void initIMU(TM_MPU6050_Device_t deviceID){

	TM_MPU6050_Result_t result=1;
	GPIO_SetBits(GPIOD, LED_ALL);
  while(result){
    if(deviceID==TM_MPU6050_Device_0){
    	result = TM_MPU6050_Init(&rawRoamerStruct, deviceID, TM_MPU6050_Accelerometer_2G, TM_MPU6050_Gyroscope_250s);
    	if(result==TM_MPU6050_Result_DeviceNotConnected || result==TM_MPU6050_Result_DeviceInvalid)
    	 result = !TM_MPU6050_Result_Ok;
    }
    if(deviceID==TM_MPU6050_Device_1){
      result = TM_MPU6050_Init(&rawClickerStruct, deviceID, TM_MPU6050_Accelerometer_2G, TM_MPU6050_Gyroscope_250s);
      if(result==TM_MPU6050_Result_DeviceNotConnected || result==TM_MPU6050_Result_DeviceInvalid)
      result = !TM_MPU6050_Result_Ok;
    }
  }
	GPIO_ResetBits(GPIOD, LED_ALL);
}

void readIMU(TM_MPU6050_t *dat){
	TM_MPU6050_ReadAll(dat);
	dat->Gyroscope_X += MPU6050_GYRO_SENS_250;
}

void getRealValues(TM_MPU6050_t *dat,sensorValue *val){
	val->gyroMult = MPU6050_GYRO_SENS_250;
	val->acceMult = MPU6050_ACCE_SENS_2;
	val->accelX = dat->Accelerometer_X/val->acceMult;
	val->accelY = dat->Accelerometer_Y/val->acceMult;
	val->accelZ = dat->Accelerometer_Z/val->acceMult;
	val->gyroX = dat->Gyroscope_X/val->gyroMult;
	val->gyroY = dat->Gyroscope_Y/val->gyroMult;
	val->gyroZ = dat->Gyroscope_Z/val->gyroMult;
}

void returnOrientation(float *pitch, float *roll, TM_MPU6050_Device_t dev){
	TM_MPU6050_t rawStruct;
	if(dev==TM_MPU6050_Device_0)
  {readIMU(&rawRoamerStruct); rawStruct=rawRoamerStruct;}
	if(dev==TM_MPU6050_Device_1)
  {readIMU(&rawClickerStruct); rawStruct=rawClickerStruct;}
  accelR1[0]=rawStruct.Accelerometer_X; accelR1[1]=rawStruct.Accelerometer_Y; accelR1[2]=rawStruct.Accelerometer_Z;
  gyroR1[0]=rawStruct.Gyroscope_X; 	gyroR1[1]=rawStruct.Gyroscope_Y; 	gyroR1[2]=rawStruct.Gyroscope_Z;
  complementaryFilter(accelR1, gyroR1,pitch,roll,dev);
}
