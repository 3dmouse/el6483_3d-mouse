#include "calib.h"

static int cflag=1;
static float	initRoamerPitch=0.0, initRoamerRoll=0.0;
static float	initClickerPitch=0.0, initClickerRoll=0.0;


void initButton() {
    NVIC_InitTypeDef NVIC_InitStructure;
    EXTI_InitTypeDef EXTI_InitStructure;
    GPIO_InitTypeDef GPIO_InitStructure;

    // Enable GPIOA clock for User button peripheral on GPIOA
    RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);
    // Enable SYSCFG clock for interrupts
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_SYSCFG, ENABLE);

    // Configure PA0 pin as input
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_DOWN;
    GPIO_Init(GPIOA, &GPIO_InitStructure);

    // Connect EXTI Line0 (external interrupt line 0) to PA0 pin
    SYSCFG_EXTILineConfig(EXTI_PortSourceGPIOA, EXTI_PinSource0);

    // Configure the interrupt using library functions
    EXTI_InitStructure.EXTI_Line = EXTI_Line0;
    EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
    EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Rising_Falling;
    EXTI_InitStructure.EXTI_LineCmd = ENABLE;
    EXTI_Init(&EXTI_InitStructure);

    // Enable and set priorities for the EXTI0 interrupt in NVIC
    NVIC_InitStructure.NVIC_IRQChannel = EXTI0_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0x01;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0x01;
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStructure);
}

void calibrate(TM_MPU6050_Device_t dev){
  initButton();
  if(dev==TM_MPU6050_Device_0)
  init_SlowBlink(LED_GREEN);
  if(dev==TM_MPU6050_Device_1)
  init_SlowBlink(LED_BLUE);
  while(cflag);
  stop_Blink();
  if(dev==TM_MPU6050_Device_0)
  returnOrientation(&initRoamerPitch,&initRoamerRoll,dev);
  if(dev==TM_MPU6050_Device_1)
  returnOrientation(&initClickerPitch,&initClickerRoll,dev);
  cflag=1;
  //setbuf(stdin,NULL);
  //printf("C Pitch: %2f, Roll: %2f\n", initPitch,initRoll);
}

void returnCalibratedOrientation(float *pitch, float *roll, TM_MPU6050_Device_t dev){
  returnOrientation(pitch,roll,dev);
  //setbuf(stdin,NULL);
  //printf("rC Pitch: %2f, Roll: %2f\t", initPitch,initRoll);
  if(dev==TM_MPU6050_Device_0){
    *pitch-=initRoamerPitch;
    *roll-=initRoamerRoll;
  }
  if(dev==TM_MPU6050_Device_1){
    *pitch-=initClickerPitch;
    *roll-=initClickerRoll;
  }
  //printf("rC Pitch: %2f, Roll: %2f\n", *pitch,*roll);
}

void EXTI0_IRQHandler(void) {
    if(EXTI_GetITStatus(EXTI_Line0) != RESET){
      cflag=0;
    }

 EXTI_ClearITPendingBit(EXTI_Line0);
}
