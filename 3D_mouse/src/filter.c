#include "filter.h"

void complementaryFilter(int16_t *accData, int16_t *gyrData, float *pitch, float *roll, TM_MPU6050_Device_t dev){
	    float pitchAcc, rollAcc, alpha;
			//int aP=1,aR=0,a=2,gP=0,gR=1;
			int aP,aR,a,gP,gR;
			if(dev==TM_MPU6050_Device_0)
			{aP=0;aR=2;a=1;gP=2;gR=0;}
			if(dev==TM_MPU6050_Device_1)
			{aP=0;aR=1;a=2;gP=1;gR=0;}
			//else
			//{aP=1;aR=0;a=2;gP=0;gR=1;}
			alpha = (TIMECONST)/(TIMECONST+INTERVAL);

	    // Integrate the gyroscope data -> int(angularSpeed) = angle
	    *pitch += ((float)*(gyrData+gP) / MPU6050_GYRO_SENS_250) * INTERVAL/1000; // Angle around the X-axis
	    *roll += ((float)*(gyrData+gR) / MPU6050_GYRO_SENS_250) * INTERVAL/1000;    // Angle around the Y-axis

	    // Compensate for drift with accelerometer data if !bullshit
	    // Sensitivity = -2 to 2 G at 16Bit -> 2G = 32768 && 0.5G = 8192
	    int forceMagnitudeApprox = fabs(*(accData+0)) + fabs(*(accData+1)) + fabs(*(accData+2));
	    if (forceMagnitudeApprox > 8192 && forceMagnitudeApprox < 32768){
		      // Turning around the X axis result+s in a vector on the Y-axis
	        pitchAcc = atan2f((float) *(accData+aP), (float) *(accData+a)) * 180 / PI;
	        *pitch = *pitch * alpha + pitchAcc * (1-alpha);

		      // Turning around the Y axis results in a vector on the X-axis
	        rollAcc = (-1)*atan2f((float) *(accData+aR), (float) *(accData+a)) * 180 / PI;
	        *roll = *roll * alpha + rollAcc * (1-alpha);
	    }
}
