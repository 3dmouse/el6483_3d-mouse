#include "leds.h"

int BlinksCnt = 1; //number of blinks in each cycle
int OffCnt = 1; // number of times the light should be off for each cycle
int BlinkInterval = 1; // 1x - delay in terms of 50ms - i.e the delay between successvice blinks - 1 - 50ms, 2- 100ms etc..
uint16_t leds = LED_ALL;

/**
 * @brief Set up on-board LEDs as GPIO outputs
 *
 * This function initializes the GPIO output pins connected
 * to the four on-board LEDs.
 */
void initLeds() {
    RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOD, ENABLE);  //RCC_AHB1PeriphClockCmd enables the clock on GPIOD

    GPIO_InitTypeDef gpio;                                 // Declared gpio as a GPIO_InitTypeDef
    GPIO_StructInit(&gpio);                                // Called GPIO_StructInit, passing a pointer to gpio
                                                           // This resets the GPIO port to its default values
                                                           // Before calling GPIO_Init,
    gpio.GPIO_Pin = LED_ALL;                                  // gpio.GPIO_Pin is used to set the pins you are interested in
    gpio.GPIO_Mode = GPIO_Mode_OUT;                        // gpio.GPIO_Mode is used to set these pins to output mode
    gpio.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_Init(GPIOD, &gpio);                               // Now GPIO_Init is called with the correct arguments
}

void initTimer2(){
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2, ENABLE);
  TIM_TimeBaseInitTypeDef timerInitStructure;
  // The APB1 bus operates at 42 MHz
  // (System clock of 168 MHz / 4 APB1 prescaler, configured in system_stm32f4xx.c)
  // Since the APB1 prescaler > 1, the timer operates at double this, 84 MHz
  // Using a prescaler of 8400 - 1, we set the timer frequency to 82 MHz/8400 kHz = 10kHz - 100us
  timerInitStructure.TIM_Prescaler = 8400 - 1;//21000 - 1;//
  timerInitStructure.TIM_CounterMode = TIM_CounterMode_Up;
  // Configure timer to trigger every 500 cycles,
  // now we should get an interrupt every 50 milli second
  timerInitStructure.TIM_Period = 500 - 1;//4000 - 1;//
  timerInitStructure.TIM_ClockDivision = TIM_CKD_DIV1;
  timerInitStructure.TIM_RepetitionCounter = 0;
  TIM_TimeBaseInit(TIM2, &timerInitStructure);
  TIM_Cmd(TIM2, ENABLE);
  //TIM_Cmd(TIM2, DISABLE);
  //TIM_ITConfig(TIM2, TIM_IT_Update, ENABLE);

  NVIC_InitTypeDef nvicStructure;
  nvicStructure.NVIC_IRQChannel = TIM2_IRQn;
  nvicStructure.NVIC_IRQChannelPreemptionPriority = 0;
  nvicStructure.NVIC_IRQChannelSubPriority = 1;
  nvicStructure.NVIC_IRQChannelCmd = ENABLE;
  NVIC_Init(&nvicStructure);
}

void init_BlinkTwice(uint16_t l){

    TIM_ITConfig(TIM2, TIM_IT_Update, DISABLE);
    BlinksCnt = 2;
    OffCnt = 6;
    leds=l;
    TIM_ITConfig(TIM2, TIM_IT_Update, ENABLE);
}

void init_Blink(uint16_t l){
  TIM_ITConfig(TIM2, TIM_IT_Update, DISABLE);
  BlinksCnt = 1;
  OffCnt = 0;
  leds=l;
  TIM_ITConfig(TIM2, TIM_IT_Update, ENABLE);
}

void init_SlowBlink(uint16_t l){
  TIM_ITConfig(TIM2, TIM_IT_Update, DISABLE);
  BlinksCnt = 1;
  OffCnt = 0;
  BlinkInterval = 5;
  leds=l;
  TIM_ITConfig(TIM2, TIM_IT_Update, ENABLE);
}

void init_TripleBlink(uint16_t l){
  TIM_ITConfig(TIM2, TIM_IT_Update, DISABLE);
  BlinksCnt = 3;
  OffCnt = 6;
  BlinkInterval = 2;
  leds=l;
  TIM_ITConfig(TIM2, TIM_IT_Update, ENABLE);
}

void TIM2_IRQHandler()
{
  static int index = 0;
  static int repeatCnt = 1; // 1x delay between blinks
  if (TIM_GetITStatus(TIM2, TIM_IT_Update) != RESET)
  {
    TIM_ClearITPendingBit(TIM2, TIM_IT_Update);
    if(--repeatCnt == 0){
      repeatCnt = BlinkInterval;
      if(index < 2*BlinksCnt){
        GPIO_ToggleBits(GPIOD, leds);
        index++;
      }
      else {
        GPIO_ResetBits(GPIOD, leds);
        index++;
      }
      if(index >= 2*BlinksCnt + OffCnt){
          index = 0;
          return;
      }
    }
  }
}

void stop_Blink(){
  GPIO_ResetBits(GPIOD,LED_ALL);
  TIM_ITConfig(TIM2, TIM_IT_Update, DISABLE);
}

void disp_error(){
	TIM_Cmd(TIM2, DISABLE);
  TIM_ITConfig(TIM2, TIM_IT_Update, DISABLE);
  GPIO_ResetBits(GPIOD,LED_ALL);
	GPIO_SetBits(GPIOD,LED_RED);
	while (1);
}
