#ifndef USE_STDPERIPH_DRIVER
#define USE_STDPERIPH_DRIVER
#endif

#include <stdio.h>

#include "stm32f4xx.h"
#include "tm_stm32f4_usb_hid_device.h"

#include "leds.h"
#include "time.h"
#include "accel.h"
#include "filter.h"
#include "mouse.h"
#include "calib.h"
#include "move.h"

float 		pitch, roll;
TM_USB_HIDDEVICE_Mouse_t Mouse;

int main(){
  SystemInit();
  initLeds();
  initSystick();
  initTimer2();
  initIMU(TM_MPU6050_Device_0);
  initIMU(TM_MPU6050_Device_1);
  delay(INTERVAL);
  calibrate(TM_MPU6050_Device_0);
  calibrate(TM_MPU6050_Device_1);
  initHIDMouse(&Mouse);
  initMove();

  while(1){
  	getMouseMoment(&Mouse);
    if(sendMouseDescriptor(&Mouse))
      disp_error();
	}
}
