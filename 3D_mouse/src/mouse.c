#include "mouse.h"

int initHIDMouse(TM_USB_HIDDEVICE_Mouse_t *Mouse){

  init_TripleBlink(LED_ALL);
  /* Initialize USB HID Device */
  TM_USB_HIDDEVICE_Init();

  /* Set default values for mouse struct */
	TM_USB_HIDDEVICE_MouseStructInit(Mouse);

  while(TM_USB_HIDDEVICE_GetStatus() != TM_USB_HIDDEVICE_Status_Connected){
  	delay(100);
    }
  stop_Blink();
  return 0;
}

int sendMouseDescriptor(TM_USB_HIDDEVICE_Mouse_t *Mouse){
  TM_USB_HIDDEVICE_MouseSend(Mouse);
  if(TM_USB_HIDDEVICE_GetStatus() != TM_USB_HIDDEVICE_Status_Connected)
    return 1;
  return 0;
}
