#include "move.h"

#include <stdio.h>

#define BOUNCECONST	8
#define JUMP				40
#define LEFTCLICKJUMP				30
#define LEFTCLICKCOUNT		 5
#define RIGHTCLICKJUMP				40
#define RIGHTCLICKCOUNT		 5

int flag = 1;
int count = 8;

float 		pitch1, roll1;
float 		pitch2, roll2;
float     deb_pitch2, deb_roll2;

void initMove(){
  initTimer4();
  pitch1 =  pitch2 = roll1 =  roll2 = 0;
}

void getMouseMoment(TM_USB_HIDDEVICE_Mouse_t *Mouse){
  count = 8;
  flag =1;
  int flag_leftclick = 0, flag_rightclick=0;
  int temp;
  int8_t    Xaxis=0, Yaxis=0;
  TIM_Cmd(TIM4, ENABLE);
  TIM_ITConfig(TIM4, TIM_IT_Update, ENABLE);
  while (count != 0) {
    if(!flag){
      count--;
      returnCalibratedOrientation(&pitch1,&roll1,roamer);
      //printf("%f\t%f\n",pitch1,roll1 );
      returnCalibratedOrientation(&pitch2,&roll2,clicker);
      flag_leftclick = isLeftButtonPressed();
      flag_rightclick = isRightButtonPressed();
      //printf("P1: %2f\tR1: %2f\t",pitch1, roll1);
      //printf("C: %d\tP2: %2f\tR2: %2f\n",flag_leftclick, pitch2, roll2);
      temp = whereMovement(mpitch, roamer);
      if(temp != 255){
        Yaxis = -1*temp;

      }
      temp = whereMovement(mroll, roamer);
      if(temp != 255)
      Xaxis = temp;
      flag =1;
    }
  }
  TIM_Cmd(TIM4, DISABLE);
  TIM_ITConfig(TIM4, TIM_IT_Update, DISABLE);
  //setbuf(stdout, NULL);
  //printf("%d\t%d\n",Xaxis,Yaxis);//,deb_pitch2,deb_roll2 );
	Mouse->XAxis = Xaxis;
	Mouse->YAxis = Yaxis;
  if(flag_leftclick)
    Mouse->LeftButton = TM_USB_HIDDEVICE_Button_Pressed;
  else Mouse->LeftButton = TM_USB_HIDDEVICE_Button_Released;
  if(flag_rightclick)
    Mouse->RightButton = TM_USB_HIDDEVICE_Button_Pressed;
  else Mouse->RightButton = TM_USB_HIDDEVICE_Button_Released;
}

int isLeftButtonPressed(){
  static int flag=0;
  static int counter=LEFTCLICKCOUNT;
  if(fabs(pitch2 - pitch1) > LEFTCLICKJUMP){
    flag= 1;
    counter=LEFTCLICKCOUNT;
  }
  else
    if(--counter==0)
    {
      flag=0;
    }

  return flag;
}

int isRightButtonPressed(){
  static int flag=0;
  static int counter=RIGHTCLICKCOUNT;
  if(fabs(roll2)-fabs(roll1) > RIGHTCLICKJUMP
     && fabs(pitch2 - pitch1) < LEFTCLICKJUMP){
    flag= 1;
    counter=RIGHTCLICKCOUNT;
  }
  else
    if(--counter==0)
    {
      flag=0;
    }

  return flag;
}
int whereMovement(a axis, mpu device){
	float *ptr;
	int i;
	if(device==roamer){
		if(axis==mpitch){
			ptr = &pitch1;
			i=0;
		}
		else{
			i=1;
			ptr = &roll1;
		}
	}
	else{
		if(axis==mpitch){
			ptr = &pitch2;
			i=2;
		}
		else{
			i=3;
			ptr = &roll2;
		}
	}
	static float old[4]= {0,0,0,0};
	static int pc[4] = {BOUNCECONST,BOUNCECONST,BOUNCECONST,BOUNCECONST};
	if(fabs(old[i]-*ptr)>JUMP){
		pc[i] = BOUNCECONST;
    old[i] = *ptr;

	}else{
		if(--pc[i]<=0){
			pc[i] = BOUNCECONST;
			float tmp = old[i];
      old[i] = *ptr;
      if(i == 0 || i == 1)
			   return scale(tmp);
		}
	}
	return 255;
}

int8_t scale(int val){
	return (int8_t)(val*127.0/80.0);
}


void initTimer4() {
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM4, ENABLE);

    TIM_TimeBaseInitTypeDef timerInitStructure;
    // The APB1 bus operates at 42 MHz
    // (System clock of 168 MHz / 4 APB1 prescaler, configured in system_stm32f4xx.c)
    // Since the APB1 prescaler > 1, the timer operates at double this, 84 MHz
    // Using a prescaler of 21000 - 1, we set the timer frequency to 82 MHz/21 kHz = 4kHz
    timerInitStructure.TIM_Prescaler = 8400 - 1;//21000 - 1;//
    timerInitStructure.TIM_CounterMode = TIM_CounterMode_Up;
    // Configure timer to trigger every 4000 cycles,
    // now we should get an interrupt every 1 second
    timerInitStructure.TIM_Period = 10 - 1;//4000 - 1;//
    timerInitStructure.TIM_ClockDivision = TIM_CKD_DIV1;
    timerInitStructure.TIM_RepetitionCounter = 0;
    TIM_TimeBaseInit(TIM4, &timerInitStructure);

    NVIC_InitTypeDef nvicStructure;
    nvicStructure.NVIC_IRQChannel = TIM4_IRQn;
    nvicStructure.NVIC_IRQChannelPreemptionPriority = 0;
    nvicStructure.NVIC_IRQChannelSubPriority = 1;
    nvicStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&nvicStructure);
}

void TIM4_IRQHandler()
{
    if (TIM_GetITStatus(TIM4, TIM_IT_Update) != RESET)
    {
        TIM_ClearITPendingBit(TIM4, TIM_IT_Update);
        flag = 0;
    }
}
