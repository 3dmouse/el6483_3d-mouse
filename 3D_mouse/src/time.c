#include "time.h"

void initSystick() {
	millisecondCounter=0;
	SysTick_Config(SystemCoreClock/1000);
}

void delay(uint32_t delay_value){
	uint32_t lastTime = millisecondCounter;
	while(millisecondCounter < lastTime + delay_value);
}

void SysTick_Handler(void){ millisecondCounter++; }
