![](http://i1068.photobucket.com/albums/u448/preethamroy/layer1_zpsw1rhrotr.png)
![](http://i1068.photobucket.com/albums/u448/preethamroy/layer2_zpse5qlg18q.png)


STATES
======
States| Explanation|
------|----------
STABLE|Mouse hovering at one point
UP|Mouse moving UP
DOWN|Mouse moving down
LEFT|Mouse moving left
RIGHT|Mouse moving right
UP-RIGHT|Mouse diagonally moving up and right
UP-LEFT|Mouse diagonally moving up and left
DOWN-LEFT|Mouse diagonally moving down and left
DOWN-RIGHT|Mouse diagonally moving down and right
CLICK|Mouse clicks left button and holds the button
WAIT|Wait 100ms before reading any transitions
DOUBLE
CLICK|Mouse double clicks left button and holds the button there

TRANSITIONS
===========
Transitions| Explanation|
-----------|-------------
LF|Left tilt of input device
RF|Right tilt of input device
UF|Upward tilt of input device
DF|Downward tilt of input device
CL|Clicking and holding of left mouse button
RL|Release and hold button
TIMEOUT|Transition after timeout runs out
