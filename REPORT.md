Project Report for EL6483
==========================

##3D Mouse, a bi-axial gesture controlled PC mouse


Abstract
===================
The aim of the project for a hand held device to be able to implement all the functions of the traditional mouse, by exclusively using simple hand gestures. No flat surface to be used. The orientation of the index finger will be mapped onto the onscreen movement and interaction of the mouse. The relative orientation of the two fingers, the index finger and the thumb of the gloves, controls the movement of the mouse and other functionality like clicks and scroll. All buttons and scroll will be replaced by simple gestures.

Introduction
===================

The pointing device has been used in GUI based operating systems since its inception, and throughout its history it has advanced through many different types of technologies. The very early devixe used a tracking ball to measure the direction the mouse box moved. Subsequent advancements in technology have upgraded the mouse device to shift to an optical tracker. But the gist of the pointing device remains the same, translate the changes in two seperate axes into the movement of the mouse in the bi-axial plane of the computer screen and to read seperate interactive elements to emulate clicking of the pointer on-screen. This means that any apparatus that is able to take these measurements will be able to control the pointing device. By shifting this process to a new (better?) technology will give the use a yet unexplored experience of operating their personal computing devices. The 3D Mouse aims to provide a new dimension to the operation of the historically used desktop mouse.

The idea of using an accelerometer device to control a pointing device was first explored a long time ago. As far back as 1988, Tektronix, Inc. published a [patent](http://www.freepatentsonline.com/4787051.html) where they used  "accelerometers for producing output signals of magnitudes proportional to the translational acceleration of the mouse in three non-parallel directions". The device was never commercialised and presently there are no devices marketed as a computer mouse which is controlled by accelerometers. But academic projects throughout the years have used orientation measuring devices to control PC mice. In 2012, a students made a "[Glove Mouse](http://people.ece.cornell.edu/land/courses/ece4760/FinalProjects/f2012/as986_hl525/as986_hl525/index.htm)" which used an accelerometer to move the mouse and touch buttons on the finger for the clicks. There aremany other projects and prototypes which use accelerometers or similar motion sensing devices to control the mouse. But what we observed was almost all of them used either a single accelerometer and implemented noise reduction in software or used physical touch buttons for the clicking functionality.

We decided to add two more (new?) features to this well used idea. First, we explored the possibility of using an [IMU](http://en.wikipedia.org/wiki/Inertial_measurement_unit)(Inetial Measurement Unit) which is an accelerometer along with a gyroscope embedded on a single chip instead of just an accelerometer. The idea here was to use the linear acceleration components along different axis to find the angular displacement with respect to the direction of the gravitational force(towards the ground!) using the accelerometer and the angular acceleration, measured in the same plane as the angle measured by the accelerometer using the gyroscope to find the relative angular movement of the measuring device in that plane. Since initial experiments were emcouraging, we implemented a filter to combine these values and provide us with very neat pitch and roll values. So the *tilt* of the device was used to translate mouse motion. Secondly, we planned to design a completely orientation based device. Hence to do away with the mouse buttons used by various predecessors to implement mouse clicking we decided to use a second IMU device. Here the relative orientation of these devices is going to decide whether the mouse buttons are pressed or released.

At this stage we felt the project had reached a reasonable level of complexity for the given time frame. But we had several ideas which we put a pin on including multiple gesture recognition "pinch and zoom", window drag, special keboard functions(WIN button, ALT, RETURN), etc.

 Hardware
 ===================
 The MPU6050 product specifications can be found  
[here](http://dlnmh9ip6v2uc.cloudfront.net/datasheets/Components/General%20IC/PS-MPU-6000A.pdf).


![](http://i1068.photobucket.com/albums/u448/preethamroy/Screenshot%20from%202015-05-09%20195103_zpsxef696mb.jpg)

AD0			-			If pin is low, I2C address is 0xD0, if pin is high, the address is 0xD2

![](http://i1068.photobucket.com/albums/u448/preethamroy/Circuitdiagram_zpsg2oodgr0.png)


 Software
 ===================
 A few of the functions of the code were implemented using 3rd party libraries as listed below,
 * [MPU6050 library](http://stm32f4-discovery.com/2014/10/library-43-mpu-6050-6-axes-gyro-accelerometer-stm32f4/) - Used to read the raw output of the MPU device to the STM board
 *   [USB HID Device](http://stm32f4-discovery.com/2014/09/library-34-stm32f4-usb-hid-device/) - Used to send a mouse descriptor structure containg all the relevant information to the computer using USB HID

 Note: No changes were made to these libraries, they were used as is, only their specific function was useful to us.

Instructions
===================
- Flash the code using the make flash onto discovery
- Connect the Sensor Modules as shown in the below figure to the STM board
- The sensors should be placed on fingers as shown below
![](http://i375.photobucket.com/albums/oo191/abhipil/mouse_zpsn4g2mpje.jpg)
- Verify the Green LED is blinking slowly indicating the calibration state
- Press the blue button once
- A Blue LED indicates calibration for the second sensor
- Press the blue button once again
- The triple blink indicates the connection phase to the PC using the micro USB. If not connected use the micro USB to connect the board to the Windows PC
- Once the thriple blink of all the 4 LED's stop you have the full control over the mouse of the PC
- If an Red LED stares at you any time.. Just reset the board :) - Its just an connection error.


Discussion
===================

3D Mouse is an Real Time Embedded Project where time critical implementation is necessary for smooth device operation. The device which is to be  ultimately used by the common user, brought to our attention the need for making our system more resilient to human errors.


Deciding on the Sensors:
-----------------------------
Quite an amount of research led to know that the basic idea of 3D mouse was implemented earlier but it was not intuitive as we wanted to make. The preceding projects used buttons, bend sensors and accelerometers and a whole lot of other devices where the usability of the device does not improve. This led us to thinking about implementing the whole system using just two sensors.

Since we had planned on using an IMU to measure orientation, we started looking for devices that best matched our need. After extensive research we chose the very popular MPU6050 - 6 axis IMU from Invensense. Since the IC itself was complicated to use we used a popular breakout board for the device called GY-521. The dimensions of this board very neatly fit into our project requirements, slightly smaller than the nail of the index finger where we were planning to place the device.

![](http://i61.tinypic.com/1pc9dz.jpg)

Hunt for Existing resources:
---------------------------------
The next major step included on reading the datasheet and creating a library to use GY-521(MPU6050) on STM Discovery. But we were fortunate to find the [library](http://stm32f4-discovery.com/2014/10/library-43-mpu-6050-6-axes-gyro-accelerometer-stm32f4/) for MPU6050 developed by Tilen Majerle for STM32 chips.

Now with the MPU libraries in hand, the next step was finding on how to send the mouse descriptor to the PC from the STM. The Demo examples on the STM and another library ([USB HID Device](http://stm32f4-discovery.com/2014/09/library-34-stm32f4-usb-hid-device/)) by Tilen Majerle, could help us know the parameters and the type of the values needed to be send to use the accelerometers as the mouse.

MakeFile is a Boon
-------------------------------------
Makefile creation was a major step, we identified the dependencies for each library and the standard STM libraries we used for interrupts, timers etc.. and added them on the source list and the include fields with flags required for successful compilation.

USB HID Device library comes with a huge list of dependencies which were available from [here](https://github.com/MaJerle/stm32f429) along with those of MPU.

Hello World!
---------------------------------
As usual make the hello world of each module work individually.

**MPU-6050:**
A through reading through the library usage documentation was merely sufficient to play around with the sensor. A detailed reference and the understanding of each individual module was required. As our application was time critical and sensitivity critical, we needed to explore the options of using the sensors to match our requirements which led us more to dig deep into understanding of the sensitivity of the sensor and the frequency of update. Lucky enough the library provided with the options for selecting the sensitivity of the sensor which was +/-2g and the default conversion frequency (1 KHz Acc and 8KHz - Gyro).

We used the I2C3 with pin set 0 (SDA - PA8,SCL- PC9) and the mpu address when the AD0 pin is LOW 0x68 (0XD0 = 0x68 << 1) and wrote a  **test function** where the sensor data is polled continuously.

**USB-HID**:
Testing of USB HID was one more huge step. The TM library available did not provide much of description or examples on using in the settings we desired to. The USB header files did miss a lot of standard header files which took quite a lot of time on identifying and making a simple main file work.

The sysconfig file used by the library provider and the one we used did involve quite a lot of difference where additional care was taken so that the library would still work with the 168MHz clock frequency which was previously configured.

The next major hurdle here involved in understanding the HID descriptor created by the library. It took quite a lot of time in realizing the descriptor and the library would work directly only on the Microsoft windows PC without any device driver, while it requires lot of tweaking to make it work in the ubuntu. Though we were able to produce the right click, left click and the scroll with little tweak we could not get the pointer move on the ubuntu OS. The tweak or the driver required would be updated once we are able to fix this issue.
For now the whole functionality of the mouse works only on the windows PC.


Filter them!
-------------
Since we were now able to read raw accelerometer and gyroscopes values and send hard coded mouse descriptors to move the pointer, we were ready to integrate the modules and use the IMU inputs to provided scaled output to mouse descriptor. This was not as easy as it seemed. The accelerometer is a very jittery device. Since it measures all linear accelerations including the tiny ones made by our hand while we hold the device, there is a very constant low frequency fluctuation of the output values. This made the mouse movement very unstable indeed. The gyroscope output was very accurate since it measured the angular acceleration and integrated the output over very small intervals to give the angular displacement. The catch here was since we were integrating the raw outputs after some time the constants of integration added up and the values started drifting from what it should be returning.

The good news was that both devices had complementary drawbacks. The popular solution used by smartphones and navigation systems is the [Kalman filter](http://www.cs.unc.edu/~welch/kalman/). Since our project is not safety critical and a slight error will not result in any real damage(hopefully!) we decided to use a simpler filter to solve our problems. The accelerometer output was passed through a low-pass filter and the gyroscope value through a high pass filter. This simple(kind of) solution solves the drawbacks of both devices to some extent. The math behind this aptly named, complimentary filter, is given [here](http://www.geekmomprojects.com/gyroscopes-and-accelerometers-on-a-chip/)

The filter gave a slightly stable output which was enough for us to translate and scale into the values to be passed to the mouse descriptor.

Its all about States
-------------
Finally the time arrived where the whole system as a whole needed to be designed. The complete system from the problem statement was taught out in terms of state as shown below.

![](http://i59.tinypic.com/e9b33o.png)

![](http://i59.tinypic.com/vfuvpu.png)


Modularize
-------------

*"If you cant explain it simply, You dont understand it well enough" *- Albert Einstein

Since we were a team working on different parts of the project simultaneously wanted to be able to read each others code without expending a lot of effort or taking up time explaining the code. Hence readability of the code was made a priority. Well if we could not make our code readable then its not a well thought out one.
We needed to make sure, whoever reads the code they would easily understand the logic. After all its an open source era. Now we were convinced on separating the code into individual modules where each of the files specifically did what they were named after.

Calibrate
-------------
Every individual is different. Nobody uses a pointing device in the same manner. In fact people rarely use the device in the same manner twice. This brought to us the need for calibration. Here the reference points for the stable position where the mouse would never move when the hand was kept at this position comfortably.


Light them up!
-------------
We all know how annoying it would be to keep waiting indefinitely, when there is no progress bar or a progress bar which doesn't move for a long time.
We used the LED's on board to indicate states to users, we used timers and blinked the LED's in a definite pattern for each state. ConcurrentIy with the process doing the movement.


Test, Test and Test
-------------
*Green’s law: If a system is designed to be tolerant to a set of faults, there will always exist an idiot so skilled to cause a nontolerated fault.*

Frankly its never enough. As is clear from the high profile examples of Toyota, the Mars Rover and Therac 25.

Every Module was tested individually and at each step where it was integrated with the whole system.

A "few" bugs were found and were fixed but we certainly expecting a lot of aditional bugs and errors once the device is in the hands of the end user.


Results
===================

![](http://i1068.photobucket.com/albums/u448/preethamroy/Roamer%20down_zpsamp07ce1.png)
![](http://i1068.photobucket.com/albums/u448/preethamroy/Roamer%20left_zpsusxu39yx.png)
![](http://i1068.photobucket.com/albums/u448/preethamroy/Roamer%20right_zpsfcynrlxp.png)
![](http://i1068.photobucket.com/albums/u448/preethamroy/Romer%20up_zps6cvmjwbq.png)
![](http://i1068.photobucket.com/albums/u448/preethamroy/Screenshot%20from%202015-05-16%20033522_zpsxnsc0n1x.png)
![](http://i1068.photobucket.com/albums/u448/preethamroy/Screenshot%20from%202015-05-16%20033559_zpscr4yqsw5.png)

Conclusion
===================
A soft real time system was modeled to recognise gestures created by analysing the orientation of two seperate IMU devices. These gestures were mapped to USB HID mouse decriptors to control the pointer on the computer screen. The project used many ideas and tricks that were learnt in the lab. The use of the accelerometer itself was motivated by the lab experiments. Debouncing of the mouse clicks, i2c communication software engineering practices were adopted as a result of the foresight obtained from the lab experiments. The final working is can be viewed
[![Youtube](http://img.youtube.com/vi/hRBRKZSEh9Q/0.jpg)](http://www.youtube.com/watch?v=hRBRKZSEh9Q)

[Link to YouTube Video](https://www.youtube.com/watch?v=hRBRKZSEh9Q)


On closing notes, we would like to mention that we had a great time engineering the project. IMUs are certainly a very challenging sensor to implement in a real time environment but it was also very rewarding. If anything the strict time restraints were the most disappointing factor in the project. Given a more flexible timeline, we would have explored the possibility of adding many more gestures to perform specific HID inputs. We definitely would have used a wireless communication system to increase the ease of use. A final consideration was (very distant) though was converting the device into a thing of the internet(IOT). Since the device is basically an input device why limiting ourselves to simply controlling a mouse. Multplicity of gestures can be used to control not just the mouse in the computer but other IOT devices in the household(or workplace).

## Team
  | Name                     | Email
--|--------------------------|---------------------
1.| Abhishek Pillai          | ap4395@nyu.edu
2.| Dhishan Amaranath        | da1683@nyu.edu
3.| Karthik Hosakere Suresh  | khs308@nyu.edu
4.| Preetham Roy Pothakamuri | prp295@nyu.edu
