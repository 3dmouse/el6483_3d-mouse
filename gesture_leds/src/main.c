#ifndef USE_STDPERIPH_DRIVER
#define USE_STDPERIPH_DRIVER
#endif

#include <stdio.h>
#include <math.h>

#include "stm32f4xx.h"
#include "stm32f4xx_gpio.h"
#include "stm32f4xx_rcc.h"
#include "stm32f4xx_tim.h"
#include "tm_stm32f4_mpu6050.h"

#include "tm_stm32f4_usb_hid_device.h"

#define PITCH_THRESHOLD		90
#define ROLL_THRESHOLD		30
#define COUNT 						10
#define PI			3.141592
#define INTERVAL 		5
#define TIMECONST		95
#define BOUNCECONST	20
#define JUMP				25

// Saves sensor readings in g and deg/s
typedef struct {
	/* Private */
	float gyroMult;         /*!< Gyroscope corrector from raw data to "degrees/s". Only for private use */
	float acceMult;         /*!< Accelerometer corrector from raw data to "g". Only for private use */
	/* Public */
	float accelX; 		/*!< Accelerometer value X axis */
	float accelY; 		/*!< Accelerometer value Y axis */
	float accelZ;	 	/*!< Accelerometer value Z axis */
	float gyroX;     	/*!< Gyroscope value X axis */
	float gyroY;    	/*!< Gyroscope value Y axis */
	float gyroZ;     	/*!< Gyroscope value Z axis */
	float temper;       	/*!< Temperature in degrees */
} sensorValue;

typedef enum {
	left = 0x00,
	stable = 0x01,
	right = 0x02,
	up = 0x03,
	down = 0x04
} s;

typedef enum {
	positive = 0x00,
	negative = 0x01,
	flicker = 0x02,
} m;

typedef enum {
	mpitch = 0x00,
	mroll = 0x01,
} a;

typedef enum {
	roamer = 0x00,
	clicker = 0x01,
} mpu;

s state = stable;
m movement = flicker;

static volatile uint32_t 	millisecondCounter;
const uint16_t 			LEDS = GPIO_Pin_12 |
				GPIO_Pin_13 |
				GPIO_Pin_14 |
				GPIO_Pin_15;				// Select all LEDS
const uint16_t 			LED_left = GPIO_Pin_12; 		// Green LEDS
const uint16_t 			LED_up = GPIO_Pin_13; 			// Orange LEDS
const uint16_t 			LED_right= GPIO_Pin_14; 		// Red LEDS
const uint16_t 			LED_down = GPIO_Pin_15; 		// Blue LEDS
const uint16_t 			npins = 4;
const uint16_t 			pin[4] = {12,13,14,15};
float 				yaw, pitch1, roll1,pitch2,roll2;
sensorValue			Roamer1;
sensorValue			Roamer2;
TM_MPU6050_t 			dats1;
TM_MPU6050_t 			dats2;

unsigned long 			interval; 				//interval since previous samples


static void 			initSystick();
void 				initLeds();
void 				delay(uint32_t );
void 				setLeds(mpu);
TM_MPU6050_Result_t 		initMPU(TM_MPU6050_t *, TM_MPU6050_Device_t);
void 				readMPU(TM_MPU6050_t *);
void 				getRealValues(TM_MPU6050_t *,sensorValue *);
void 				ComplementaryFilter(int16_t *, int16_t *, float *, float *);
m		whereMovement(a, mpu);
//void ComplementaryFilter1(int16_t *, int16_t *, float *, float *, float*);


void initSystick() {
	millisecondCounter=0;
	SysTick_Config(SystemCoreClock/1000);
}

void SysTick_Handler(void) { millisecondCounter++; }

int main(){
	float *p1,*r1,*p2,*r2;
	p1=&pitch1; r1=&roll1;
	p2=&pitch2; r2=&roll2;
	int16_t accelR1[3]={0,0,0};
	int16_t gyroR1[3]={0,0,0};

	int16_t accelR2[3]={0,0,0};
	int16_t gyroR2[3]={0,0,0};

	TM_USB_HIDDEVICE_Mouse_t Mouse;

	SystemInit();
	initSystick();
	initLeds();
	//initTimer2();
	/* Initialize USB HID Device */
	TM_USB_HIDDEVICE_Init();
	// Initialise MPU

	/* Set default values for mouse struct */
	TM_USB_HIDDEVICE_MouseStructInit(&Mouse);
	/*
	while(initMPU(&dats, TM_MPU6050_Device_0)){
		GPIO_SetBits(GPIOD, LED_up);
		delay(10000);
	}
	*/
	while(1){

	while(TM_USB_HIDDEVICE_GetStatus() != TM_USB_HIDDEVICE_Status_Connected){
		GPIO_SetBits(GPIOD,LED_down);
		GPIO_ResetBits(GPIOD,LED_right);
		delay(10);
		}
		GPIO_SetBits(GPIOD,LED_right);
		GPIO_ResetBits(GPIOD,LED_down);
		int counter = 0;
		Mouse.XAxis = 100;
		//Mouse.YAxis = 100;
		Mouse.Wheel = -10;
		//Mouse.YAxis = 10;
    //Mouse.YAxis = -100;
		//Mouse.LeftButton = 0x01;
		TM_USB_HIDDEVICE_MouseReleaseAll();
		delay(10);
		for(counter = 0; counter < 1; counter++){
			if(TM_USB_HIDDEVICE_MouseSend(&Mouse) == TM_USB_HIDDEVICE_Status_Connected){
				GPIO_SetBits(GPIOD,LED_left);
				delay(10);
				TM_USB_HIDDEVICE_MouseReleaseAll();
			}
			else GPIO_ResetBits(GPIOD,LED_left);
			//delay(10);
		}
		//Mouse.Wheel = 00;
		//Mouse.RightButton = 0x01;
		//TM_USB_HIDDEVICE_MouseSend(&Mouse);
		//delay(50);
		TM_USB_HIDDEVICE_MouseReleaseAll();
		GPIO_ResetBits(GPIOD,LED_left);
		while(1);
	}

	while(1) {
		delay(INTERVAL);
		setbuf(stdout, NULL);
		// Read raw sensor data
		readMPU(&dats1);
		readMPU(&dats2);
/*		printf("A:: X: %d Y: %d Z: %d 	G:: X: %d Y: %d Z: %d\t\t\t",		// STATUS.Working, prints raw values read from MPU
		dats.Accelerometer_X, dats.Accelerometer_Y, dats.Accelerometer_Z,
		dats.Gyroscope_X, dats.Gyroscope_Y, dats.Gyroscope_Z
					);
*/
		accelR1[0]=dats1.Accelerometer_X; accelR1[1]=dats1.Accelerometer_Y; accelR1[2]=dats1.Accelerometer_Z;
		gyroR1[0]=dats1.Gyroscope_X; 	gyroR1[1]=dats1.Gyroscope_Y; 	gyroR1[2]=dats1.Gyroscope_Z;

		accelR2[0]=dats2.Accelerometer_X; accelR2[1]=dats2.Accelerometer_Y; accelR2[2]=dats2.Accelerometer_Z;
		gyroR2[0]=dats2.Gyroscope_X; 	gyroR2[1]=dats2.Gyroscope_Y; 	gyroR2[2]=dats2.Gyroscope_Z;
		ComplementaryFilter(accelR1, gyroR1,p1,r1);
		ComplementaryFilter(accelR2, gyroR2,p2,r2);
		getRealValues(&dats1, &Roamer1);
		getRealValues(&dats2, &Roamer2);

/*		setbuf(stdout, NULL);
  		printf("A:: X: %.2f Y: %.2f Z: %.2f 	G:: X: %.2f Y: %.2f Z: %.2f\t\t\t\t",	// Status.Working, prints mg and deg/s values
						Roamer1.accelX*1000, Roamer1.accelY*1000, Roamer1.accelZ*1000,
						Roamer1.gyroX, Roamer1.gyroY, Roamer1.gyroZ
					);
		setbuf(stdout, NULL);
			printf("A:: X: %.2f Y: %.2f Z: %.2f 	G:: X: %.2f Y: %.2f Z: %.2f\t\t\t\t",	// Status.Working, prints mg and deg/s values
						Roamer2.accelX*1000, Roamer2.accelY*1000, Roamer2.accelZ*1000,
						Roamer2.gyroX, Roamer2.gyroY, Roamer2.gyroZ
					);	// the member variables for each direction
*/
	  setLeds(roamer);
		//printf("Pitch1: %f Roll1: %f\t",pitch1, roll1);						// Status.Incorrecct, values do not make sense
		//printf("\tPitch2: %f Roll2: %f\n", pitch2, roll2);						// Status.Incorrecct, values do not make sense
	}
}

void initLeds() {				// Initialises LEDS
    RCC->AHB1ENR |= RCC_AHB1ENR_GPIODEN;	// Enable GPIOD Clock
    unsigned int i=npins;

    while(i>0) {
			GPIOD->MODER |= (1 << 2*pin[i-1]);			// Set GPIOD pin 12-15
			GPIOD->MODER &= ~(1 << (2*pin[i-1]+1));			// mode to output
			i--;
    }
}


void setLeds(mpu roamer){
	switch(state){
		case stable :
			GPIO_ResetBits(GPIOD, LEDS);
			movement=whereMovement(mroll,roamer);
			if(movement!=flicker){
				if(movement==positive)
					state = left;
				if(movement==negative)
					state = right;
			}
			movement=whereMovement(mpitch,roamer);
			if(movement!=flicker){
				if(movement==positive)
					state = up;
				if(movement==negative)
					state = down;
			}
			break;
		case left :
			GPIO_SetBits(GPIOD, LED_left);
			if(whereMovement(mroll,roamer)==negative)
				state = stable;
			break;
		case right :
			GPIO_SetBits(GPIOD, LED_right);
			if(whereMovement(mroll,roamer)==positive)
				state = stable;
			break;
		case up :
			GPIO_SetBits(GPIOD, LED_up);
			if(whereMovement(mpitch,roamer)==negative)
				state = down;
			if(whereMovement(mpitch,roamer)==flicker)
				state = stable;
			break;
		case down :
			GPIO_SetBits(GPIOD, LED_down);
			if(whereMovement(mpitch,roamer)==positive)
				state = stable;
			break;
	}

}

m whereMovement(a axis, mpu device){
	float *ptr;
	int i;
	if(device==roamer){
		if(axis==mpitch){
			ptr = &pitch1;
			i=0;
		}
		else{
			i=1;
			ptr = &roll1;
		}
	}
	else{
		if(axis==mpitch){
			ptr = &pitch2;
			i=0;
		}
		else{
			i=1;
			ptr = &roll2;
		}
	}
	static float old[2]= {0,0};
	static int pc[2] = {BOUNCECONST,BOUNCECONST};
	if(old[i]!=*ptr){
		if(fabs(*ptr-old[i])>JUMP){
			//printf("hey %f ", old[i]);
			if(--pc[i]==0){
				if(*ptr-old[i]>0){
					old[i]=*ptr;
					return positive;
				}
				else{
					old[i]=*ptr;
					return negative;
				}
			}
	  }
		else
			pc[i]=BOUNCECONST;
	}
	return flicker;
}

void delay(uint32_t delay_value){
	uint32_t lastTime = millisecondCounter;
	while(millisecondCounter < lastTime + delay_value);
}

TM_MPU6050_Result_t initMPU(TM_MPU6050_t *dat, TM_MPU6050_Device_t deviceID){
	TM_MPU6050_Result_t result;
	result = TM_MPU6050_Init(dat, deviceID, TM_MPU6050_Accelerometer_2G, TM_MPU6050_Gyroscope_250s);
	if(result==TM_MPU6050_Result_DeviceNotConnected || result==TM_MPU6050_Result_DeviceInvalid)
	 return !TM_MPU6050_Result_Ok;
	return result;
}

void readMPU(TM_MPU6050_t *dat){
	TM_MPU6050_ReadAll(dat);
	dat->Gyroscope_X += MPU6050_GYRO_SENS_250;
}

void getRealValues(TM_MPU6050_t *dat,sensorValue *val){

	val->gyroMult = MPU6050_GYRO_SENS_250;
	val->acceMult = MPU6050_ACCE_SENS_2;
	val->accelX = dat->Accelerometer_X/val->acceMult;
	val->accelY = dat->Accelerometer_Y/val->acceMult;
	val->accelZ = dat->Accelerometer_Z/val->acceMult;
	val->gyroX = dat->Gyroscope_X/val->gyroMult;
	val->gyroY = dat->Gyroscope_Y/val->gyroMult;
	val->gyroZ = dat->Gyroscope_Z/val->gyroMult;
}

void ComplementaryFilter(int16_t *accData, int16_t *gyrData, float *pitch, float *roll){
	    float pitchAcc, rollAcc, alpha;
			static int aP=1,aR=2,a=0,gP=2,gR=1;
			alpha = (TIMECONST)/(TIMECONST+INTERVAL);

	    // Integrate the gyroscope data -> int(angularSpeed) = angle
	    *pitch += ((float)*(gyrData+gP) / MPU6050_GYRO_SENS_250) * INTERVAL/1000; // Angle around the X-axis
	    *roll += ((float)*(gyrData+gR) / MPU6050_GYRO_SENS_250) * INTERVAL/1000;    // Angle around the Y-axis

	    // Compensate for drift with accelerometer data if !bullshit
	    // Sensitivity = -2 to 2 G at 16Bit -> 2G = 32768 && 0.5G = 8192
	    int forceMagnitudeApprox = fabs(*(accData+0)) + fabs(*(accData+1)) + fabs(*(accData+2));
	    if (forceMagnitudeApprox > 8192 && forceMagnitudeApprox < 32768){
		// Turning around the X axis result+s in a vector on the Y-axis
	        pitchAcc = atan2f((float) *(accData+aP), (float) *(accData+a)) * 180 / PI;
	        *pitch = *pitch * alpha + pitchAcc * (1-alpha);

		// Turning around the Y axis results in a vector on the X-axis
	        rollAcc = (-1)*atan2f((float) *(accData+aR), (float) *(accData+a)) * 180 / PI;
	        *roll = *roll * alpha + rollAcc * (1-alpha);
	    }
}


/*

void ComplementaryFilter1(int16_t *accData, int16_t *gyrData, float *Rx, float *Ry, float *Rz) {
	    float x, y, z, alpha;
			alpha = (TIMECONST)/(TIMECONST+INTERVAL);
	    // Integrate the gyroscope data -> int(angularSpeed) = angle
	    *Rx += ((float)*(gyrData) / MPU6050_GYRO_SENS_250) * INTERVAL/1000; // Angle around the X-axis
	    *Ry += ((float)*(gyrData+1) / MPU6050_GYRO_SENS_250) * INTERVAL/1000;    // Angle around the Y-axis
			*Ry += ((float)*(gyrData+1) / MPU6050_GYRO_SENS_250) * INTERVAL/1000;    // Angle around the Y-axis

	    // Compensate for drift with accelerometer data if !bullshit
	    // Sensitivity = -2 to 2 G at 16Bit -> 2G = 32768 && 0.5G = 8192
	    int forceMagnitudeApprox = fabs(*(accData+0)) + fabs(*(accData+1)) + fabs(*(accData+2));
	    if (forceMagnitudeApprox > 8192 && forceMagnitudeApprox < 32768){
		// Turning around the X axis results in a vector on the Y-axis
	        pitchAcc = atan2f((float) *(accData+1), (float) *(accData+2)) * 180 / PI;
	        *pitch = *pitch * alpha + pitchAcc * (1-alpha);

		// Turning around the Y axis results in a vector on the X-axis
	        rollAcc = atan2f((float) *(accData+0), (float) *(accData+2)) * 180 / PI;
	        *roll = *roll * alpha + rollAcc * (1-alpha);
	    }
}
*/
/*//The typical mouse pointer update speed is usually 125 Hz but can range upto 1000 Hz.
///This Timer2 is configured to interrupt every 8ms i.e 125 HZ to update the mouse - i.e every 8ms
void initTimer2(){
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2, ENABLE);
	TIM_TimeBaseInitTypeDef timerInitStructure;
// The APB1 bus operates at 42 MHz
// (System clock of 168 MHz / 4 APB1 prescaler, configured in system_stm32f4xx.c)
// Since the APB1 prescaler > 1, the timer operates at double this, 84 MHz
// Using a prescaler of 8400 - 1, we set the timer frequency to 82 MHz/8400 kHz = 10kHz - 100us
timerInitStructure.TIM_Prescaler = 8400 - 1;
timerInitStructure.TIM_CounterMode = TIM_CounterMode_Up;
// Configure timer to trigger every 80 cycles,
// now we should get an interrupt every 81 milli second
timerInitStructure.TIM_Period = 80 - 1;
timerInitStructure.TIM_ClockDivision = TIM_CKD_DIV1;
timerInitStructure.TIM_RepetitionCounter = 0;
TIM_TimeBaseInit(TIM2, &timerInitStructure);
TIM_Cmd(TIM2, ENABLE);

TIM_ITConfig(TIM2, TIM_IT_Update, ENABLE);

NVIC_InitTypeDef nvicStructure;
nvicStructure.NVIC_IRQChannel = TIM2_IRQn;
nvicStructure.NVIC_IRQChannelPreemptionPriority = 0;
nvicStructure.NVIC_IRQChannelSubPriority = 1;
nvicStructure.NVIC_IRQChannelCmd = ENABLE;
NVIC_Init(&nvicStructure);
}

///Timer 2 ISR. TO read the values and update the mouse pointer
void TIM2_IRQHandler()
{
//Read the sensor
//update the  and y co-ordinate
//read the mouse click pointer
//send the HID to the device
}*/
